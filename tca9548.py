"""

   tca9548.py

    Support for I2C port expander (TCA9548A, PCA9548A)

This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2023-2024 Rob Hamerling

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

Rob Hamerling, Version 4.0, January 2024

"""

from machine import I2C, Pin
import time

TCA9548_I2C_ADDR = const(0x70)              # with A0,A1,A2 pulled low (or left open)

class TCA9548():
    """Class to represent an TCA9548A I2C port extender.
    """
    def __init__(self, i2c, addr=TCA9548_I2C_ADDR, reset_pin=None):
        """ Initialize TCA9548 at specified I2C address.
            <bus> active I2C instance
            <reset_pin> Pin number for the reset signal of TCA9548A
        """
        self._connected = False             # not connected
        if not isinstance(i2c, I2C):        # rough check for I2C
            print(f"{i2c} must be an I2C instance")
            return
        self._bus = i2c                     # I2C bus
        self._addr = addr                   # I2C address of TCA9548
        self._reset = reset_pin             # reset pin
        if reset_pin is not None:           # specified
            if isinstance(reset_pin, Pin):  # Pin instance
                self._reset = reset_pin
            else:                           # presumably a pin number
                self._reset = Pin(reset_pin, Pin.OUT)
        self._buffer = bytearray(1)         # I/O buffer for I2C writes
        self._channels = 0x00               # state of all 8 channels: inactive
        self.reset()

    def reset(self) -> bool:
        """ hardware reset
            return connection state
        """
        if self._reset is not None:
            self._reset.off()
            time.sleep_us(1)                # reset
            self._reset.on()
        self._channels = 0x00               # state of all 8 channels: inactive
        return self._write(self._channels)  # set initial state

    def _write(self, value) -> bool:
        """ Write the specified byte value
            return connection state
        """
        self._buffer[0] = value
        try:
            if (acks := self._bus.writeto(self._addr, self._buffer)) == 1:
                self._connected = True      # (still) connected
            else:
                print(f"Addr 0x{self._addr:02X}: acks received: {acks} (1 expected)")
                self._connected = False     # not conected (anymore)
        except:
            print(f"Addr 0x{self._addr:02x}: I2C write error")
            self._connected = False
        return self._connected              # actual connection status

    def channel(self, channel=0) -> bool:
        """ activate or deactivate a single channel
            <channel> number (0..7)
            Note: other channel(s) will be deactivated
            return connection state
        """
        self._channels = (1 << (channel & 0x07))   # limit 0..7 
        # print(f"Selecting channel {channel}: mask 0x{self._channels:02X}") 
        return self._write(self._channels)

    @property
    def isconnected(self) -> bool:
        """ return connection state """
        return self._connected

#
