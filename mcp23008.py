"""

   mcp23008.py

   Support for port expander MCP23008
   (restricted to output-only function of MCP23008 pins)

This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2023-2024 Rob Hamerling

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

Rob Hamerling, Version 4.0, January 2024

"""

from machine import Pin, I2C
from time import sleep_us

MCP23008_I2C_ADDR = const(0x20)         # with A0,A1,A2 pulled low

MCP23008_IODIR    = const(0x00)
MCP23008_IPOL     = const(0x01)
MCP23008_GPINTEN  = const(0x02)
MCP23008_DEFVAL   = const(0x03)
MCP23008_INTCON   = const(0x04)
MCP23008_IOCON    = const(0x05)
MCP23008_GPPU     = const(0x06)
MCP23008_INTF     = const(0x07)
MCP23008_INTCAP   = const(0x08)
MCP23008_GPIO     = const(0x09)
MCP23008_OLAT     = const(0x0A)

class MCP23008():
    """ Class for MCP23008 GPIO extender.
    """
    def __init__(self, i2c, addr=MCP23008_I2C_ADDR, init=0x00, reset_pin=None):
        """ Initialize MCP23008 at specified I2C address.
            <bus> active I2C instance
            <init> initial state of all pins
            <reset_pin> Pin for reset of MCP23008
        """
        self._connected = False             # not connected yet
        if not isinstance(i2c, I2C):        # check for I2C
            print(f"{i2c} must be an I2C instance")
            return
        self._bus = i2c                     # I2C bus
        self._addr = addr                   # I2C address of MCP23008
        self._pins = init                   # initial state all 8 pins
        self._reset = reset_pin             # reset pin
        self._buffer = bytearray(2)         # pre-allocated I/O buffer
        if reset_pin is not None:           # specified
            if isinstance(reset_pin, Pin):  # Pin instance
                self._reset = reset_pin
            else:                           # pin number
                self._reset = Pin(reset_pin, Pin.OUT)
        self.reset()                        # reset MCP23008

    def reset(self) -> bool:
        """ hardware reset
            return connection state
        """
        if self._reset is not None:             # reset pin specified in instance
            self._reset.off()
            sleep_us(1)                         # hardware reset
            self._reset.on()
        self.write_reg(MCP23008_IODIR, 0x00)    # all pins output
        self.write_reg(MCP23008_IOCON, 0x00)    # default configuration OK
        return self.write_reg(MCP23008_OLAT, self._pins)    # initial pin settings

    def write_reg(self, reg, value=0x00) -> bool:
        """ Write the specified byte value to the specified register
            return connection state
        """
        self._buffer[0] = reg
        self._buffer[1] = value
        try:
            if (acks := self._bus.writeto(self._addr, self._buffer)) != 2:
                print(f"MCP23008 addr 0x{self._addr:02x}: acks received: {acks} (2 expected)")
                self._connected = False     # not conected (anymore)
            else:
                self._connected = True      # (still) connected
        except:
            print(f"MCP23008 reg 0x{reg:02X}, data 0x{value:02X} to addr 0x{self._addr:02X}: error")
            self._connected = False
        return self._connected              # connection status

    def pins(self, value=0x00) -> bool:
        """ set all pins in one operation
            return connection state
        """
        self._pins = value
        return self.write_reg(MCP23008_OLAT, self._pins)

    def pin(self, offset=0, value=True) -> bool:
        """ set an individual pin on or off
            <offset> represents pin number (0..7)
            <value> set pin high (True) or low (False)
            wiring of the LEDs may require use of negative logic
            return connection state
        """
        offset &= 0x07                      # limit offset to 0..7
        if value == True:
            self._pins |= (1 << offset)     # on
        else:
            self._pins &= (~(1 << offset))  # off
        return self.write_reg(MCP23008_OLAT, self._pins)

    @property
    def isconnected(self) -> bool:
        """ return connection state """
        return self._connected

#
