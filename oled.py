# oled.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2023 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""


"""
 Rob Hamerling, Version 3.4, April 2023

 Simple Micropython driver for OLEDS with I2C interface
 Text-mode only, fixed 8x8 font for 64x128 pixels OLEDs.
 OLEDs with either SSD1306 or SH1106 chips are supported
 (64x132 pixels handled as 64x128 pixels).
 Text lines from top-to-bottom or bottom-to-top (arbitrary!)
 Normal or reversed video, contrast control.

"""
from micropython import const
from oled_font import *

OLED_I2C_ADDRESS            = const(0x3C)       # default for SH1106 or SSD1306

OLED_COMMAND_MODE           = const(0x00)
OLED_DATA_MODE              = const(0x40)

# commands for OLED control
OLED_SET_COLUMN_NUMBER_LOW  = const(0x00)
OLED_SET_COLUMN_NUMBER_HIGH = const(0x10)
OLED_SET_MEMORY_ADDRESSING  = const(0x20)
OLED_STOP_SCROLLING         = const(0x2E)
OLED_CHARGE_PUMP_VOLTAGE    = const(0x30)
OLED_SET_START_LINE_NUMBER  = const(0x40)
OLED_SET_CONTRAST           = const(0x81)
OLED_CHARGE_PUMP            = const(0x8D)
OLED_NON_FLIP_HORIZONTAL    = const(0xA0)
OLED_DISPLAY_ALL_ON_RESUME  = const(0xA4)
OLED_DISPLAY_ALL_ON         = const(0xA5)
OLED_NON_INVERTED_DISPLAY   = const(0xA6)
OLED_SET_MULTIPLEX          = const(0xA8)
OLED_DISPLAY_OFF            = const(0xAE)
OLED_DISPLAY_ON             = const(0xAF)
OLED_PAGE_START_ADDRESS     = const(0xB0)
OLED_NON_FLIP_VERTICAL      = const(0xC0)
OLED_SET_DISPLAY_OFFSET     = const(0xD3)
OLED_SET_DISPLAY_CLOCK_DIV  = const(0xD5)
OLED_SET_PRECHARGE          = const(0xD9)
OLED_SET_COM_PINS           = const(0xDA)
OLED_SET_V_COM_DESELECT     = const(0xDB)

# initialization commands (seems to work for both SH1106 and SSD1306!)
commands = (
        OLED_COMMAND_MODE,                      # command mode
        OLED_DISPLAY_OFF,                       # 0xAE
        OLED_SET_DISPLAY_CLOCK_DIV,             # 0xD5
        0x80,                                   # POR value
        OLED_SET_MULTIPLEX,                     # 0xA8
        63,                                     # # of vertical pixels - 1
        OLED_SET_DISPLAY_OFFSET,                # 0xD3
        0x00,                                   # no offset: line 0
        OLED_SET_START_LINE_NUMBER | 0x00,      # 0x40, line 0
        OLED_CHARGE_PUMP,                       # 0x8D (SSD1306)
        0x14,                                   # with internal Vcc
        OLED_CHARGE_PUMP_VOLTAGE | 0x02,        # 0x30, POR value (8.0V)
        OLED_SET_MEMORY_ADDRESSING,             # 0x20
        0x02,                                   # page addressing
        OLED_NON_FLIP_HORIZONTAL,               # 0xA0, (SEG scan direction left to right)
        OLED_NON_FLIP_VERTICAL,                 # 0xC0, (COM scan direction upside up!)
        OLED_SET_COM_PINS,                      # 0xDA
        0x12,                                   # for 128x64 display
        OLED_SET_CONTRAST,                      # 0x81
        0xCF,                                   # in range 0x00..0xFF
        OLED_SET_PRECHARGE,                     # 0xD9
        0x22,                                   # POR value
        OLED_SET_V_COM_DESELECT,                # 0xDB
        0x30,                                   # valid value for SH1106 and SSD1306
        OLED_STOP_SCROLLING,                    # 0x2E
        OLED_NON_INVERTED_DISPLAY,              # 0xA6, white/blue on black
        OLED_DISPLAY_ON,                        # 0xAF
        OLED_DISPLAY_ALL_ON_RESUME              # 0xA4
    )


class OLED():
    def __init__(self, i2c, addr=OLED_I2C_ADDRESS):
        self._bus = i2c
        self._addr = addr
        self._pixel_offset = 0                 # becomes 2 with 132x64 oledsw
        self._connected = False                # 'True' to be determined!
        self._cmdbuffer = bytearray(2)
        self._cursorbuffer = bytearray(4)
        self._fontbuffer = bytearray(9)
        try:
            self._bus.writeto(self._addr, bytearray(commands))    # setup hardware
            self._connected = True             # connected!
            self._query_status()
            self.clear()
        except:
            print("==> Failed to connect to OLED!")

    def _query_status(self):
        # read status register to obtain chip type (for pixel_offset)
        try:
            status = self._bus.readfrom(self._addr,1)[0] & 0x0F   # lower nibble
            # print("status byte 0x{:02x}".format(status))
            if status == 0x06 or status == 0x08:    # probably a 132 pixels wide OLED
                self._pixel_offset = 2             # to center text on the line
        except:
            self._connected = False

    def _write_buffer(self, buffer):
        # display character(s) only when connected.
        if self._connected:
            try:
                self._bus.writeto(self._addr, buffer)
            except Exception as err:
                print(err)
                self._connected = False            #

    def _write_cmd(self, cmd):
        # issue a command
        self._cmdbuffer[0] = OLED_COMMAND_MODE               # Co=0, D/C#=1
        self._cmdbuffer[1] = cmd
        self._write_buffer(self._cmdbuffer)

    def flip(self, flag=True):
        # set display direction top-to-bottom (False) or bottom-to-top (True)
        self._write_cmd(OLED_NON_FLIP_HORIZONTAL | (0x01 if flag == True else 0x00))
        self._write_cmd(OLED_NON_FLIP_VERTICAL | (0x08 if flag == True else 0x00))

    def contrast(self, contrast):
        # set contrast (range 0..255)
        self._write_cmd(OLED_SET_CONTRAST)
        self._write_cmd(contrast)

    def reverse_video(self, flag=True):
        # black on white (True) or white on black (False)
        self._write_cmd(OLED_NON_INVERTED_DISPLAY | (flag & 1))

    def cursor(self, row=0, column=0):
        # set cursor at (row, column)
        self._cursorbuffer[0] = OLED_COMMAND_MODE
        self._cursorbuffer[1] = OLED_PAGE_START_ADDRESS | (row & 0x07)      # 8 lines!
        pixcol = column * OLED_FONT_WIDTH + self._pixel_offset
        self._cursorbuffer[2] = OLED_SET_COLUMN_NUMBER_LOW | (pixcol & 0x0F)
        self._cursorbuffer[3] = OLED_SET_COLUMN_NUMBER_HIGH | (pixcol >> 4)
        self._write_buffer(self._cursorbuffer)

    def clear(self, startline=None, endline=None, *, reverse_video=False):  # line numbers (0..7)
        # clear line(s) of screen
        # no startline: erase whole screen
        # only startline: erase single line
        # startline and endline: erase range of lines, incl endline
        # endline must be larger than startline (no check!)
        # reverse video causes display of the reverse of the current
        # video setting as set with the 'reverse_video()' method
        first = 0
        last = OLED_DISPLAY_LINES
        if not startline is None:
            first = startline & 0x07            # limit to range 0..7
            last = first + 1                    # default: one line to be erased
        if not endline is None:
            last = (endline & 0x07) + 1         # limit to range 1..8
        for i in range(first, last):            # range of lines
            self.cursor(i, 0)                   # begin of line
            self.char(" ", OLED_DISPLAY_CHARS, reverse_video=reverse_video)  # one whole line of blanks
        self.cursor(first, 0)                   # back to startline, column 0

    def char(self, ch, repeat=1, *, reverse_video=False):
        # draw the pixel pattern of a single char and increment cursor position
        # the character can be written multiple times (e.g. to fill a line)
        ch = ord(ch)                            # ordinate of character
        if not OLED_FONT_ORD_MIN < ch < OLED_FONT_ORD_MAX:  # not within font table
            ch = OLED_FONT_ORD_MIN              # use 'space' in stead
        self._fontbuffer[0] = OLED_DATA_MODE   # data
        self._fontbuffer[1:8] = oled_font[ch - OLED_FONT_ORD_MIN]  # 7 pixel columns
        self._fontbuffer[8] = 0x00             # last pixel column empty
        if reverse_video:
            for i in range(1,9):                # font pattern
                self._fontbuffer[i] = self._fontbuffer[i] ^ 0xFF   # flip all bits of a pixel column
        for _ in range(repeat):                 # once or repeatedly
            self._write_buffer(self._fontbuffer)  # draw 8 pixel columns

    def string(self, buffer, row=None, column=0, *, reverse_video=False):
        # display string beginning at current cursor position
        # or at specified row and column
        if row is not None:
            self.cursor(row, column)
        for ch in buffer:                       # all chars in buffer
            self.char(ch, reverse_video=reverse_video)   # one char at a time

    @property
    def connected(self):
        return self._connected

    @property
    def address(self):
        return self._addr


#
