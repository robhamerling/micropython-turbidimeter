# main.py

""" Elementary tests of Turbidimeter hardware """

import gc                               # memory management
import time                             # extended time module
import os
import oled                             # display (text-only) driver
from oled_font import *                 # font and its properties
from machine import I2C, Pin, Signal, Timer, freq, unique_id
from sys import exit
from struct import unpack

# dictionary of devices and their I2C addresses
devices = { 0x20 : 'MCP23008(left)',    # A0,A1,A2 pulled low
            0x24 : 'MCP23008(right)',   # A0,A1 pulled low, A2 pulled high
            0x29 : 'TSL2591',           # IR ambient light sensor
            0x3C : 'OLED',              # display
            0x57 : 'AT24C32',           # EEPROM (on DS3231 board?)
            0x68 : 'DS3231',            # RTC module
            0x70 : 'PCA9548',           # with A0,A1,A2 open or pulled low
            }

def start_test(button):
    print("Hit button to start the test cycle,",
          "\nwaiting for maximum 5 seconds ... ", end="")
    try:
        timeout = 50                    # wait max 5 seconds
        while timeout > 0:
            if button.value() == 0:     # button pressed
                break
            time.sleep_ms(100)
            timeout -= 1
        else:
            return False                # wait time expired
    except:
        return False
    return True

def start_heartbeat(tmr, led):
    tmr.deinit()
    tmr.init(period=500, mode=Timer.PERIODIC,   # blinking LED during test
             callback=lambda x: led.off() if led.value() else led.on())
    print("Heartbeat LED is blinking during test")

def scan_I2C(bus):
    actives = bus.scan()
    print("Scanning for I2C devices:")
    for x in actives:
        print(f"   0x{x:02x} : {devices.get(x, "???"):s}")   # show addr + device (or ???)
    print()
    return actives

def show_info(bus):
    print("Show system information on OLED")
    YY, MM, DD, hh, mm, _, _, _ = time.localtime()
    mem_alloc = gc.mem_alloc()
    mem_free = gc.mem_free()
    mem_total = mem_alloc + mem_free
    stat = os.statvfs('/flash')
    blocks_total = stat[2]
    blocks_free = stat[3]
    dsp = oled.OLED(bus)                # instance with default settings
    dsp.clear()
    dsp.flip(True)
    dsp.string(f"{YY:4d}-{MM:02d}-{DD:02d} {hh:2d}:{mm:02d}", 0, 0, reverse_video=True)
    dsp.reverse_video(False)
    dsp.string(f"System {os.uname().machine.split()[-1][:8]:s}", 2, 0)
    dsp.string(os.uname().release[:OLED_DISPLAY_CHARS], 3, 0)   # max full line
    dsp.string(f"Freq. {freq()//1000000} MHz", 4, 0)
    dsp.string(f"Memfree {mem_free/mem_total * 100.0 : .1f}%", 5, 0)
    dsp.string(f"ROMfree {blocks_free/blocks_total * 100.0 : .1f}%", 6, 0)

def blink_leds(bus, mcps):
    def tomcp(reg, data):
        buf = bytearray(2)
        buf[0] = reg
        buf[1] = data
        if 0x20 in mcps:                # left MCP23008
            bus.writeto(0x20, buf)
        if 0x24 in mcps:                # right MCP23008
            bus.writeto(0x24, buf)
    print("Flashing LEDs one-by-one")
    tomcp(0x00, 0x00)                   # make all pins OUTPUT
    for _ in range(3):                  # repeat the cycle
        mask = 0x01                     # start with first LED
        for _ in range(8):
            tomcp(0x0A, mask)           # 1 LED ON
            time.sleep(.3)
            mask <<= 1                  # next LED
    tomcp(0x0A, 0x00)                   # all OFF
         
    print()

def show_sensors(bus):
    print(f"PCA9548 detected, scanning for TSL2591s")
    buf = bytearray(1)
    buf4 = bytearray(4)
    for i in range(4):                  # expect sensors at ports 0..3
        buf[0] = (1 << i)               # select PCA9548 port
        print(f"  Probing channel {i} (0x{buf[0]:02x} -> 0x70)")
        bus.writeto(0x70, buf)
        actives = bus.scan()
        if 0x29 in actives:             # I2C address of TSL2591
            print("  Probably TSL2591 present, checking ID")
            try:
                bus.readfrom_mem_into(0x29, 0xA0 | 0x12, buf)   # device ID register
                if buf[0] == 0x50:                              # ID of TSL2591
                    print(f"    TSL2591 identified at channel {i}")
                    print("    power-on, enable ALS")
                    buf[0] = 0x03                               # power on, enable ALS
                    bus.writeto_mem(0x29, 0xA0 | 0x00, buf)     # enable register
                    print("    waiting for first integration")
                    buf[0] = 0x00
                    while (buf[0] & 0x01) == 0x00:              # not ready yet
                        time.sleep_ms(50)                       # wait a little
                        bus.readfrom_mem_into(0x29, 0xA0 | 0x13, buf) # status register
                    print("    reading counts: ", end="")
                    bus.readfrom_mem_into(0x29, 0xA0 | 0x14, buf4)  # both channels
                    ch0, ch1 = unpack("<HH", buf4)              # channels 0 and 1
                    print(f"{ch0=:d}, {ch1=:d}")
                else:
                    print(f"  Failed to identify TSL2591, received ID: 0x{buf[0]:02x}")
            except Exception as e:
                print(e)
        else:
            print(f"  No TSL2591 detected on channel {i}")
    print()

board = os.uname().machine.split()[-1]
print(f"\nCheck of turbidimeter hardware {board=}")

if board in ('ESP32C3', 'ESP32-C3', 'ESP32-C3FH4'):     # board with ESP32-C3
    if unique_id() == b'\x00\x00\x00\x00\x00\x00':      # specific board TENSTAR ROBOT 
        print("   Tenstar Robot ESP32-C3")
        heartbeat_led = Signal(Pin(8, Pin.OUT), invert=False)   # external LED
        button = Pin(6, Pin.IN, Pin.PULL_UP)            # start start button
        reset_left_mcp23008 = Pin(4, Pin.OUT, value=1)  # left MCP23008 active
        reset_right_mcp23008 = Pin(5, Pin.OUT, value=1) # right MCP23008 active
        reset_pca9548 = Pin(2, Pin.OUT, value=1)        # PCA9548 active
        bus = I2C(0, scl=Pin(10), sda=Pin(9))           #
    else:
        print("   Presumably Seeed Studio XIAO ESP32-C3")
        heartbeat_led = Signal(Pin(5, Pin.OUT), invert=False)   # external LED
        button = Pin(3, Pin.IN, Pin.PULL_UP)            # start start button
        reset_left_mcp23008 = Pin(10, Pin.OUT, value=1) # left MCP23008 active
        reset_right_mcp23008 = Pin(2, Pin.OUT, value=1) # right MCP23008 active
        reset_pca9548 = Pin(8, Pin.OUT, value=1)        # PCA9548 active
        bus = I2C(0, scl=Pin(7), sda=Pin(6))            #
    tmr = Timer(2)                                      # hardware Timer (0 or 2)
elif board in ('ESP32S3', 'ESP32-S3'):                  # board with ESP32S3
    print("   Presumably Seeed Studio XIAO ESP32-S3")
    heartbeat_led = Signal(Pin(21, Pin.OUT), invert=True)   # onboard LED
    button = Pin(2, Pin.IN, Pin.PULL_UP)                # start start button
    reset_left_mcp23008 = Pin(9, Pin.OUT, value=1)      # left MCP23008 active
    reset_right_mcp23008 = Pin(1, Pin.OUT, value=1)     # right MCP23008 active
    reset_pca9548 = Pin(7, Pin.OUT, value=1)            # PCA9548 active
    bus = I2C(0, scl=Pin(6), sda=Pin(5))                #
    tmr = Timer(2)                                      # hardware Timer (0 or 2)
elif board == "RP2040":                                 # board with RP2040
    print("   Presumably Seeed Studio XIAO RP2040")
    heartbeat_led = Signal(Pin(29, Pin.OUT), invert=False)  # external LED
    button = Pin(27, Pin.IN, Pin.PULL_UP)               # start button
    reset_left_mcp23008 = Pin(3, Pin.OUT, value=1)      # left MCP23008 active
    reset_right_mcp23008 = Pin(26, Pin.OUT, value=1)    # right MCP23008 active
    reset_pca9548 = Pin(2, Pin.OUT, value=1)            # PCA9548 active
    bus = I2C(1)                                        # (defaults: scl=7, sda=6)
    tmr = Timer()                                       # use software Timer
else:
    print("Unsupported board for Turbidimeter!")
    exit(1)

try:
    if start_test(button):                      # limited wait
        print("starting")
        start_heartbeat(tmr, heartbeat_led)
        actives = scan_I2C(bus)
        if 0x68 in actives:                     # ds3231 present
            import ds3231get                    # sync RTC
        if 0x3C in actives:                     # LED
            show_info(bus)                      # show some sys_info
        if 0x20 in actives or 0x24 in actives:  # any MCP23008
            blink_leds(bus, actives)            # blink the LEDs
        if 0x70 in actives:                     # TCA9548 I2C expander
            show_sensors(bus)                   # scan for TSL2591s
    else:
        print("timeout!")
except Exception as e:
    print(e)
finally:
    tmr.deinit()
    heartbeat_led.off()

#
