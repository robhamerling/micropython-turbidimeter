# main.py

from machine import Pin
from sys import platform

# reset onboard LEDs 
if platform == 'rp2':
    # RGB LEDs off (use negative logic):
    x = Pin(16, Pin.OUT, value=1)
    x = Pin(17, Pin.OUT, value=1)
    x = Pin(25, Pin.OUT, value=1)
    # NeoPixel inactive
    x = Pin(11, Pin.OUT, value=0)   # NeoPixel power-pin OFF
elif platform == 'esp32':
    # no onboard LEDs
    pass

# synchronize RTC with DS3231
print('Setting RTC')
import ds3231get

# elementary test of Turbidimeter hardware
import hardware_check

# Turbidimeter program itself
# import turbidi

#
