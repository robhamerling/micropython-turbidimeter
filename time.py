"""
    Private Micropython 'time' library:
    modified 'localtime' method of builtin time
    Author: Rob Hamerling

    With respect to modifying or extending existing Micropython libraries see:
    section "Extending built-in libraries from Python" at the bottom of
    https://docs.micropython.org/en/latest/library/index.html

"""
import sys
_path = sys.path
sys.path = ()
try:
    from time import *                  # from builtin library
finally:
    sys.path = _path
    del _path

def localtime(tz_offset=3600, dst_offset=3600):
    """ Build localtime() from gmtime()
        - <tz_offset> number of seconds later than UTC (default 3600)
        - <dst_offset> number of seconds later (default 3600)
        Return time tuple like time.gmtime()
        RTC is assumed to run UTC
        Calculation is somewhat simplified:
        - valid for years 2000..2100
        - for Central European Standard:
          (last Sunday of March 01:00 UTC to last Sunday of October 01:00 UTC)
    """
    year = gmtime()[0]                  # current year
    DSTstart = mktime((year,3,31-(5*year//4+4)%7,1,0,0,0,0))
    DSTend = mktime((year,10,31-(5*year//4+1)%7,1,0,0,0,0))
    utc = time()                        # system time, presumably UTC
    loctim = utc + tz_offset            # adjust for timezone
    if DSTstart <= utc < DSTend:        # within daylight savings period
        loctim += dst_offset            # adjust for DST
    return gmtime(loctim)               # local time (as time tuple)

#