# tsl2591.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2024 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
Rob Hamerling, Version 4.0, January 2024

"""

# Class for TSL2591

from time import sleep_ms
from micropython import const
from struct import unpack

TSL2591_I2C_ADDR = const(0x29)                          # I2C default address

TSLREG_EAN       = const(0x00)                          # enable register
TSLREG_CFG       = const(0x01)                          # control register
TSLREG_ID        = const(0x12)                          # device ID register
TSLREG_STATUS    = const(0x13)                          # status register
TSLREG_CH0       = const(0x14)                          # Channel 0 (as word)
TSLREG_CH1       = const(0x16)                          # Channel 1

TSLCMD_NORMAL    = const(0xA0)                          # command bits

TSLCMD_POWER_OFF = const(0x00)
TSLCMD_POWER_ON  = const(0x01)
TSLCMD_ALS_AEN   = const(0x02)

TSLSTAT_AVALID   = const(0x01)                          # AVALID bit in status register

TSLGAIN_LOW      = const(0)
TSLGAIN_MEDIUM   = const(1)
TSLGAIN_HIGH     = const(2)
TSLGAIN_MAX      = const(3)

TSLGAIN_FACTOR   = (const(1), const(25), const(400), const(9900))   # (last: IR sensor)


TSLINTEG_100MS   = const(0)
TSLINTEG_200MS   = const(1)
TSLINTEG_300MS   = const(2)
TSLINTEG_400MS   = const(3)
TSLINTEG_500MS   = const(4)
TSLINTEG_600MS   = const(5)
TSLINTEG_LOW     = TSLINTEG_100MS
TSLINTEG_MAX     = TSLINTEG_600MS

TSLCOUNT_MAX = (const(37888),) + (const(65535),) * 5    # maximum counts per integration time

class TSL2591(object):
    """ TSL2591 class for ESP32
        expects active I2C interface (hardware or software)
        gain and integration time can be changed at any time by user program.
        When automatic gain adjustment is desired it must be activated separately
    """
    def __init__(self, i2c, addr=TSL2591_I2C_ADDR):
        self._bus  = i2c                               # hardware/software
        self._addr = addr                              # I2C address of TSL2591
        self._gain = TSLGAIN_MEDIUM                    # default gain
        self._integ = TSLINTEG_200MS                   # default integration time
        self._buffer1 = bytearray(1)                   # register read/write
        self._buffer4 = bytearray(4)                   # counter read
        self._identified = False                       # device at this I2C address present
        self._connected = False                        # device is probably a TSL2591
        try:
            self._buffer1[0] = 0x00                    # dummy data!
            self._bus.writeto_mem(self._addr, TSLCMD_NORMAL | TSLREG_EAN, self._buffer1)   
            self._identified = True                    # expected device present
        except:
            return
        if not self._read_register(TSLREG_ID) == 0x50:     # check for expected device_ID
            return                                      # device-ID not found
        self._connected = True
        self.set_gain_integration(self._gain, self._integ)    # activate with defaults

    def _read_register(self, reg):
        """ read register, return integer """
        try:
            self._bus.readfrom_mem_into(self._addr, TSLCMD_NORMAL | reg, self._buffer1)
            return self._buffer1[0]                            # return integer value
        except Exception as err:
            print(f"TSL2591 I2C read_register 0x{reg:02X} {err}")
            print("Identified:", self._identified, ", Connected:", self._connected)
            return -1                                   # indication 'no receive'

    def _write_register(self, reg, data):
        """ write register return True, on error: False """
        self._buffer1[0] = data
        try:
            self._bus.writeto_mem(self._addr, TSLCMD_NORMAL | reg, self._buffer1)
        except Exception as err:
            print(f"TSL2591 I2C write_register 0x{reg:02X} {err}")
            print("Identified:", self._identified, ", Connected:", self._connected)
            return False
        return True


    """ Public methods and properties """

    def read_counts(self):      
        """ read 4 consecutive bytes starting at address TSLREG_CH0
            return visual and IR counts (-1 with error or overfow)
        """
        if not self._connected:
            return (0, 0)
        try:
            self._bus.readfrom_mem_into(self._addr, TSLCMD_NORMAL | TSLREG_CH0, self._buffer4)
            viscount, ircount = unpack("<HH", self._buffer4);   # 2 USHORTs little endian
            if viscount >= self.overflow_count:     
                viscount = -1                           # overflow!
            if ircount >= self.overflow_count:     
                ircount = -1                            # overflow!
            return (viscount, ircount)
        except Exception as err:
            print("I2C read counts error", err)
            return (-1, -1)                             # probably read error

    def close(self):
        """ Power-down TSL2591 """
        self._write_register(TSLREG_EAN, TSLCMD_POWER_OFF) # deactivate device
        self._connected = False

    def set_gain_integration(self, gain, integ):
        """ set gain, integration-time (codes)
            see TSLGAIN_xxxxx and TSLINTEG_xxxxx constants above
        """
        if gain in range(TSLGAIN_LOW, TSLGAIN_MAX + 1):
            self._gain = gain
        if integ in range(TSLINTEG_100MS, TSLINTEG_600MS + 1):
            self._integ = integ
        if self._connected:
            self._write_register(TSLREG_EAN, TSLCMD_POWER_ON)      # leave power-on, deactivate ALS
            self._write_register(TSLREG_CFG, (self._gain << 4) | self._integ)       # update timing register
            self._write_register(TSLREG_EAN, TSLCMD_POWER_ON | TSLCMD_ALS_AEN)      # re-activate ALS
            while (self._read_register(TSLREG_STATUS) & TSLSTAT_AVALID) == 0x00:    # test 'busy' bit
                sleep_ms(50)
        return self._connected
                                                # awaiting first integration
    @property
    def identified(self):
        """ return if a device with expected I2C address is present """
        return self._identified                        # True/False

    @property
    def isconnected(self):
        """ return status of the connection """
        return self._connected                         # True/False

    @property
    def overflow_count(self):
        """ return overflow count for actual integration time """
        return TSLCOUNT_MAX[self._integ]

    @property
    def gain(self):
        """ return current gain code """
        return self._gain

    @property
    def gain_factor(self):
        """ return current gain factor """
        return TSLGAIN_FACTOR[self._gain]

    @property
    def integration(self):
        """ return current integration time code """
        return self._integ

    @property
    def integration_time(self):
        """ return integration time in milliseconds """
        return 100 * (1 + self._integ)



#
