#!/usr/bin/env python

# ds3231set.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2024 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
 Rob Hamerling, Version 4.0, January 2024

 Initialize DS3231 from current RTC time
 First open an online terminal session with the ESP32
 and make an instance of rtc with:
        from machine import RTC
        rtc = RTC()
 Then set rtc to the desired UTC date and time with:
        rtc.datetime( (YYYY, MM, DD, 0, hh, mm, ss, 0) )
 and check if current time is correctly set with:
        rtc.datetime()
 Then import this script to copy the RTC date/time to the DS3231
 From then on the ESP32 date/time can be set from the DS3231
 for example from main.py with the ds3231get script.
"""

from machine import I2C, Pin
import os

import ds3231

# Select I2C bus hardware and wiring dependent!
board = os.uname().machine.split()[-1]
print(f"{board=}")
if board == 'MIMXRT1062DVJ6A':          # TEENSY 4.0
    bus = I2C(1)
elif board == 'ESP32C3':
    bus = I2C(0, sda=6, scl=7)
elif board == 'ESP32S3':
    bus = I2C(0, sda=5, scl=6)
elif board == 'RP2040':
    bus = I2C(1)
else:
    print(f"Unsupported platform: {board}")

ds3231 = ds3231.DS3231(bus)             # instance of DS3231 class
if ds3231.isconnected:
    print("Setting DS3231 to current RTC values", ds3231.rtc_to_ds3231())
else:
    print("DS3231 not found!")

#
