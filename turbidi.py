# turbidi.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2024 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
 turbidi.py: turbidimeter with multiple TSL2591 sensors

 Rob Hamerling, Version 4.0, January 2024

 Description:
 Reading measurement parameters from internal file
 (or  file on SDcard) to configure the measurements.
 Light sensor tsl2591 accessed via I2C with a private library
 (note: using only IR-sensor, no lux calculations, etc.).
 Measurement results are sent to the serial terminal and
 stored in a log file on SD-card of local (virtual) disk.
 Progress is shown on serial terminal and on OLED.
 Logging time is obtained from internal clock.

 Requirements and setup:
 - Seeed XIAO ESP32-C3 or Seeed XIAO RP2040
 - TCA9548 I2C bus expander
 - 2 MCP23008 port expander
 - (max) 4 TSL2591s via I2C
 - IR LEDs (1 for each TSL2591)
 - RGB LEDs (1 for each TSL2591)
 - DS3231 via I2C to synchronize internal RTC
 - OLED via I2C to show measurement progress
 - Start-button
 - simple LED for 'heartbeat' signal

  Notes:
 - RTC must be set to UTC time
   (for local time an extension of the standard time module is provided)

   Pin assignments (GPIO numbers)
   ------------------------------
   Seeed XIAO ESP32-C3
     2  reset of MCP23008(right)
     3  start-button
     4  N/C
     5  heartbeat LED
     6  SDA (I2C data)
     7  SCL (I2C clock
     8  reset of TCA9548
     9  N/C
    10  reset of MCP23008(left)
    20  N/C
    22  N/C

   Seeed XIAO ESP32-S3
     1  reset of MCP23008(right)
     2  start-button
     3  N/C
     4  heartbeat LED
     5  SDA (I2C data)
     6  SCL (I2C clock
     7  reset of TCA9548
     8  N/C
     9  reset of MCP23008(left)
    43  N/C
    44  N/C

   Seeed XIAO RP2040
     0  N/C
     1  N/C
     2  reset of TCA9548
     3  reset of MCP23008(left)
     4  N/C
     6  SDA (I2C data)
     7  SCL (I2C clock
    26  reset of MCP23008(right)
    27  start button
    28  N/C
    29  heartbeat LED
"""

TURBIDIVERSION = "4.0"                      # version (string)

import time                                 # extended time module
import os                                    
from sys import exit
from machine import I2C, Pin, Timer, reset
#  from machine import SDCard               # SDcard interface currently not used

# support for MCP23008 GPIO expander
from mcp23008 import *

# support for TCA9548 I2C port expander
from tca9548 import *

#  support for TSL2591 light sensor
from tsl2591 import *                       # Infrared sensor (TSL2591 mini board)

#  display progress of measurements on OLED
from oled import *                          # sh1106/ssd1306 (I2C-)display

measurement_starttime = 0                   # global variable: to be set at real start!

parmfile = "turbidi.prm"                    # measurements parameterfile
logfmt = "cuvet{:d}-{:02d}{:02d}{:02d}-{:02d}{:02d}{:02d}.csv"
LED_color = ("IR", "Red", "Green", "Blue")
month = ("Jan","Feb","Mar","Apr","May", "Jun","Jul","Aug","Sep","Oct","Nov","Dec")

# Class for blinking led during measurements
# For ease of use the instance is callable to activate or deactivate the heartbeat.
class HeartBeat():
    def __init__(self, timer, led):
        self._tmr = timer
        self._led = led
        self.active(False)
    def active(self, status=True):
        self._tmr.deinit()                      # de-activate timer
        self._led.off()                         # LED off
        if status:                              # activate heartbeat                             
            self._tmr.init(period=500, mode=Timer.PERIODIC,
                callback=lambda t: self._led.off() if self._led.value() else self._led.on())
    def __call__(self, status=True):
        self.active(status)

# Place holder: instance is instantiated in main()
heartbeat = None                                # global access and callable

# Class for cuvet (configuration with sensor and LEDs)
class CUVET():
    def __init__(self, i2c, tca, channel, mcp, IRled):
        self._bus = i2c                         # instance of I2C bus
        self._tca = tca                         # instance of TCA9548
        self._channel = channel                 # TCA9548 channel (0..7) of TCS2591
        self._tsl = None                        # TCS2591 (to be detected)
        self._mcp = mcp                         # instance of MCP23008
        self._irled = IRled                     # pin-number of IR LED on MCP23008
                                                # pins of RGB led follow in RBG sequence
        self.tslcfg = [0,0,0,0,0,0,0,0]         # 4 pairs of gain and integration codes
                                                # (may vary per measurement period)
        self._status = False                    # initially not active 
        self.flog = None                        # log file object (created with open_log)
        self.detect()                           # check if TSL2591 present

    def detect(self):  
        self._tca.channel(self._channel)        # select TSL9548 channel 
        self._tsl = TSL2591(self._bus)          # instantiate TSL2591
        self._status = self._tsl.isconnected    # copy TSL2591 status
        
    def deinit(self):
        self._mcp.pins(0x00)                    # all leds off
        self.close_log()                        # done with the log
        self._status=False                      # mark as inactive

    def configure_tsl(self, gain, integ):
        if self._status:
            self._tca.channel(self._channel)    # select TSL9548 channel 
            self._tsl.set_gain_integration(gain, integ)

    @property   
    def isconnected(self):
        return self._status

    def open_log(self, comment=""):
        # build name of logfile for this cuvet and open the file
        # log format is in CSV format for later processing as spreadsheet
        # <comment> is text string to identify this measurement cycle
        if self._status:
            YY, MM, DD, hh, mm, ss, _, _ = time.localtime()
            logfile = logfmt.format(self._channel + 1, YY-2000, MM, DD, hh, mm, ss)
            # print("Logging to file", logfile)
            self.flog = open(logfile, "w")          #  start logging
            self.log(f'\n"Turbidimeter v{TURBIDIVERSION:s}"\n')     # Turbidi version
            self.log(f'"Micropython {os.uname().release}"\n')       # uPy version
            self.log(f'"{DD:2d} {month[MM-1]:s} {YY:2d}"\n')        # date      
            self.log(f'"Cuvet {self._channel + 1}"\n\n')            # Cuvet number (1..4)    
            self.log(f'"{comment}"\n\n')                            # user comment
            self.log(',,,"IR",,,,,"RED",,,,,"GREEN",,,,,"BLUE"\n\n')
            self.log('"time","secs"') 
            for _ in range(4):
                self.log(',,"gain","integ","Vis","IR"')
            self.log('\n\n')
           
    def close_log(self):
        if self.flog is not None:
            self.flog.close()
            self.flog = None

    def log(self, logline):
        # write a string to logfile
        if self.flog is not None:                   # only when log open
            self.flog.write(logline)

    def tslcounts(self):
        # Data collection (4 read-outs of TSL2591), incl logging
        if not self._status:                        # not active
            return False
        self._tca.channel(self._channel)            # select tcs9548 channel 
        time.sleep_ms(3)                            # settle (to be determined)
        _, _, _, hh, mm, ss, _, _ = time.localtime()    # time of 1st of 4 read-outs
        elapsed_time = time.ticks_diff(time.ticks_ms(), measurement_starttime)   
        self.log('{:2d}:{:02d}:{:02d}, {:d}'.format(
                hh, mm, ss,                         # actual clock time
                elapsed_time // 1000))              # in whole seconds
        for i in range(4):                          # process each LED color
            self._mcp.pin(self._irled + i)          # specific led ON (resp. IR, red, green, blue)
            self.configure_tsl(self.tslcfg[2*i], self.tslcfg[2*i+1])   # gain and integration for this LED
            counts = self._tsl.read_counts()        # counts with this LED color
            self._mcp.pins()                        # all LEDs OFF
            self.log(f",, {self._tsl.gain}, {self._tsl.integration}, {counts[0]}, {counts[1]}")
        self.log("\n")
        return True
        
def oled_datetime(oled):
    """  show local date and time on second line of OLED """
    _, MM, DD, hh, mm, ss, _, _ = time.localtime()
    oled.string(f"{DD:2d} {month[MM-1]:s}. {hh:2d}:{mm:02d}:{ss:02d}", 1, 0)

def parm_collection(parmfile, maxargs=12):              # (required number of arguments per line!)
    """ collect parameters from parameter file
        parameters will be stored in a period-list: one item for each period
        each item of this list is itself a list of parameters for one period
    """
    separators = ".,:;/"                                # separator characters -> space
    periods = []                                        # table of period parameters
    print("Processing parameter file:", parmfile)
    linenumber = 0
    try:
        with open(parmfile, "r") as pf:
            for ln in pf:                               # each line of parmfile
                linenumber += 1
                try:
                    ln = ln.strip()                     # remove leading and trailing white space
                    if ln == "" or not "0" <= ln[0] <= "9": # empty or invalid line
                        continue                        # skip
                    for x in separators:                # all separator chars
                        ln = ln.replace(x, " ")         # replaced by <space>
                    args = ln.split()                   # convert to list of arguments
                    if len(args) != maxargs:            # must contain exact number of args
                        print("Incorrect (number of) arguments, line", linenumber)
                        continue
                    args = [int(args[i]) for i in range(maxargs)]   # convert args to integers
                    for i in range(4):                   # for all LEDs
                        if not TSLGAIN_LOW <= args[2*i+4] <= TSLGAIN_MAX:   # gain
                            print("Gain code outside bounds, line", linenumber)
                            continue
                        if not TSLINTEG_LOW <= args[2*i+5] <= TSLINTEG_MAX:  # integrationtime
                            print("Integrationtime code outside bounds, line", linenumber)
                            continue
                    periods.append(args)                # add new period
                except Exception as err:
                    print(err, ", line", linenumber)
    except:
        print("Could not open", parmfile)
    return periods                                      # return table with period parameters.

def parm_display(periods, oled):
    """  Show parameters of all collected periods """
    p = 0
    print("\nMeasurement parameters:")
    print("            hh:mm            mm:ss  gain,integ....")
    for HH, MM, mm, ss, *cfgdata in periods:         # for every period
        p += 1
        print(f"{p:2d}. period: {HH:2d}:{MM:02d}, interval: {mm:2d}:{ss:02d}", end="")
        for i in range(4):
            print(f"  {LED_color[i]:s}:({cfgdata[i*2]:d},{cfgdata[i*2+1]:d})", end="")
        print()
    total_minutes = sum([p[0] * 60 + p[1] for p in periods])
    print(f"Number of periods {len(periods)}")
    print(f"Total measurement time: {total_minutes//60:d} hours, {total_minutes%60:d} minutes\n")
    oled.string(f"Periods: {len(periods):d}", 2, 0)
    oled.string(f"Runtime: {total_minutes//60:d}:{total_minutes%60:02d}", 3, 0)

def datacollection(cuvet):
    """ Data collection (read-out of all active TSL2591s)
        <cuvet> - list of cuvet object instances
    """
    for i in range(len(cuvet)):                     # all cuvets
        if cuvet[i].isconnected:                    # active cuvet
            all_counts = cuvet[i].tslcounts()       # 4 pairs of counts (a pair per LED color)
           
def measurement_cycle(cuvet, oled, periods, startbutton):
    """ Measurements, monitoring progress, logging results
        <cuvet> : a list of cuvet objects
        <oled> : an OLED instance
        <periods> : list of measurement parameter lines 
        <startbutton> : instance of start signal pin
        Returns 'True'  : measurement complete normally
                'False' : measurement interrupted or an error occured
    """
    global measurement_starttime                # (variable outside this function)

    try:
        print("Enter comment to identify the measurement")
        measurement_comment = input("> ")       # input from user
        oled_datetime(oled)
        print("Ready! Push start button...")    # 'human' request to start measurements
        oled.string("Push start...", 7, 0)
        while startbutton.value():
            time.sleep_ms(50)               # loop until button pushed
    except KeyboardInterrupt:
        print("Interrupted from keyboard!")
        oled.clear(2, 7)
        oled.string("Interrupted!", 3, 2)
        return False

    measurement_starttime = time.ticks_ms() # actual start of measurements (ms)
    heartbeat(True)                         # start blinking led
   
    #  start logging for every (active) cuvet
    for i in range(len(cuvet)): 
        cuvet[i].open_log(measurement_comment)      # open logfile for (active) cuvets
              
    period_num = 0
    # Successively handle all periods
    for HH, MM, mm, ss, *cfgdata in periods:        # new period (cfgdata is a list)
        period_num += 1
        #  time-calculations in milliseconds
        periodduration = (HH * 60 + MM) * 60 * 1000 # ms
        intervalduration = (mm * 60 + ss) * 1000    # ms
        oled.clear(2, 7)
        oled.string(f"Period {period_num:d} of {len(periods):d}", 2, 0)
        oled.string(f"Interval {intervalduration//1000:d} sec", 3, 0)
        for i in range(len(cuvet)):                 # all cuvets
            oled.string(f"Cuvet {i+1:d}: ", i + 4, 0)
            cuvet[i].tslcfg = cfgdata               # store the 4 pairs of gain/integ 
            oled.string("OK" if cuvet[i].isconnected else "--", i + 4, 8)
        print(f"Period {period_num:d} .", end="")
        periodstart = time.ticks_ms()               # start time of this period (ms)
        try:
            while time.ticks_diff(time.ticks_ms(), periodstart) < periodduration:
                intervalstart = time.ticks_ms()     # starttime of interval
                datacollection(cuvet)               # actual data collection and logging
                                                    # of all active cuvets
                # pause for the duration of the interval
                while time.ticks_diff(time.ticks_ms(), intervalstart) < intervalduration:
                    time.sleep_ms(25)               # short pause
                print(" .", end="")                 # progress
            datacollection(cuvet)                   # one more measurement at end of period
            print()                                 # new line for next period
        except KeyboardInterrupt:
            print("Interrupted from keyboard!")
            for i in range(len(cuvet)):
                cuvet[i].log('\n"Interrupted from keyboard!"\n')
                cuvet[i].deinit()                   # de-activate cuvet 
            oled.clear(2, 7)
            oled.string("Interrupted!", 3, 2)
            heartbeat(False)                        # stop heartbeat
            return False
        except Exception as err:
            print(err)
            for i in range(len(cuvet)):
                cuvet[i].log('\n"Terminated due to error!"\n')
                cuvet[i].log('"' + str(err) + '"\n')
                cuvet[i].deinit()                   # de-activate cuvet 
            oled.clear(2, 7)
            oled.string("<< Error! >>", 3, 2)
            heartbeat(False)                        # stop heartbeat
            return False
    else:                                           # all measurements completed
        oled.clear(2, 7)
        oled.string("All done!", 3, 3)
        for i in range(len(cuvet)):       
            cuvet[i].deinit()                       # de-activate cuvet
        heartbeat(False)                            # stop heartbeat
    return True

# ==============================================================================
#  Mainline
# ==============================================================================

def main():
    global heartbeat                   
    board = os.uname().machine.split()[-1]                  # Micropython chip name  
    print(f"Detected {board=:s}")
    # check for elementary requirements
    if board == 'ESP32C3':                                  # presumably XAIO ESP32C3
        startbutton = Pin(3, Pin.IN, Pin.PULL_UP)           # start button
        reset_mcp23008_left = Pin(10, Pin.OUT, value=1)     # left MCP23008, active
        reset_mcp23008_right = Pin(2, Pin.OUT, value=1)     # right MCP23008, active
        reset_tca9548 = Pin(8, Pin.OUT, value=1)            # TCA9548, active
        bus = I2C(0, scl=Pin(7), sda=Pin(6), freq=100000)   # hardware I2C
        heartbeat = HeartBeat(Timer(2), Pin(5, Pin.OUT))    # blinking LED during measurements
    elif board == 'ESP32S3':                                # presumably XIAO ESP32S3
        startbutton = Pin(2, Pin.IN, Pin.PULL_UP)           # start button
        reset_mcp23008_left = Pin(9, Pin.OUT, value=1)      # left MCP23008, active
        reset_mcp23008_right = Pin(1, Pin.OUT, value=1)     # right MCP23008, active
        reset_tca9548 = Pin(7, Pin.OUT, value=1)            # TCA9548, active
        bus = I2C(0, scl=Pin(6), sda=Pin(5), freq=100000)   # hardware I2C
        heartbeat = HeartBeat(Timer(2), Pin(4, Pin.OUT))    # blinking LED during measurements
    elif board == "RP2040":                                 # presumably XIAO RP2040
        startbutton = Pin(27, Pin.IN, Pin.PULL_UP)          # start button
        reset_mcp23008_left = Pin(3, Pin.OUT, value=1)      # left MCP23008, active
        reset_mcp23008_right = Pin(26, Pin.OUT, value=1)    # right MCP23008, active
        reset_tca9548 = Pin(2, Pin.OUT, value=1)            # TCA9548, active
        bus = I2C(1, scl=Pin(7), sda=Pin(6), freq=100000)   # hardware I2C
        heartbeat = HeartBeat(Timer(), Pin(29, Pin.OUT))    # blinking LED during measurement        
    else:
        print("Only Seeed Studio XIAO ESP32-C3, ESP32-S3 and RP2040 are supported!")
        return

    # See what I2C devices are on board
    print(f"{bus=}")
    active_devs = bus.scan()
    print("Active I2C devices:",
          " ".join([f'0x{d:02X}' for d in active_devs]))
    if len(active_devs) == 0:
        print("No I2C devices detected: terminating")
        exit(1)

    #  initialize OLED (an active I2C interface is expected)
    oled = OLED(bus)                        # instance of OLED class
    if oled.connected:                  
        oled.flip(True)                     # depends on mounting!
        oled.string("Turbidimeter " + TURBIDIVERSION, 0, 0)
    else:
        print("No OLED connected, proceeding without!")

    # gather measurement parameters 
    periods = parm_collection(parmfile, 12)         # expect 12 values per line
    if len(periods) == 0:
        print("No valid periods in", parmfile)
        oled.clear(3)
        oled.string("No periods!", 3, 0)
        return
    parm_display(periods, oled)                     # show summary of parms

    # I2C multiplexor instance
    if TCA9548_I2C_ADDR in active_devs:             # I2C muliplexor present
        tca = TCA9548(bus, reset_pin=reset_tca9548)     # instance 
    else:
        print("No TCA9548 detected, terminating")
        exit(1)

    # GPIO multiplexor instances (assumed to be present!)
    mcpleft = MCP23008(bus, 0x20, reset_pin=reset_mcp23008_left)
    mcpright = MCP23008(bus, 0x24, reset_pin=reset_mcp23008_right)

    # Build list of CUVET instances
    cuvet = [ 
              CUVET(bus, tca, 0, mcpleft, 0),       # IR LED on pin 0
              CUVET(bus, tca, 1, mcpleft, 4),       # ..        pin 4
              CUVET(bus, tca, 2, mcpright, 0),      # ..        pin 0
              CUVET(bus, tca, 3, mcpright, 4),      # ..        pin 4
            ]

    # For each CUVET instance check if TSL2591 sensor is present
    # Cuvet index is range 0..3, but presented to user as numbers 1..4
    for i in range(4):                              # all cuvets
        print(f"TSL2591[{i:d}] ", end="")
        oled.clear(3 + i)
        oled.string(f"Cuvet {i+1:d} ")
        if not cuvet[i].isconnected:
            print("not found!")
            oled.string("--")
        else:
            print("initialized!")
            oled.string("active!") 

    # Hardware checked and initialized, now start measurements
    if measurement_cycle(cuvet, oled, periods, startbutton):   
        print("\n ==> Rebooting in a few seconds...")
        time.sleep(5)
        reset()                             # (allows restart of program)
    else:                                   # measurement error or keyboard interrupt
        print("\n(no automatic restart)\n")


# =============================================

print("Turbidimeter version", TURBIDIVERSION)
main()

#
