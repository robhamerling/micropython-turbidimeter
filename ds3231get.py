#!/usr/bin/env python

# ds3231get.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2024 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 Rob Hamerling, Version 4.0, January 2024

 Initialize RTC from DS3231

 Import this script to initialize RTC from DS3231
"""

from machine import I2C
import os

import ds3231

print(f'Synchronizing RTC with DS3231:')
# Select I2C bus hardware and wiring dependent!
board = os.uname().machine.split()[-1]  # micropython base
print(f"Detected {board=:s}")
if board == 'MIMXRT1062DVJ6A':          # TEENSY 4.0
    bus = I2C(1)                        # default pins
elif board in ('ESP32', 'ESP32C3'):
    bus = I2C(0, scl=7, sda=6)          # non defailt pins
elif board == 'ESP32S3':
    bus = I2C(0, scl=6, sda=5)          # non default pins
elif board == 'RP2040':
    bus = I2C(1)                        # XIAO RP2040

ds3231 = ds3231.DS3231(bus)             # instance of DS3231 class
if ds3231.isconnected:
    Y,M,D,h,m,s = ds3231.ds3231_to_rtc()    # read date and time from DS3231
    print(f'{Y}/{M}/{D} {h:d}:{m:02d}:{s:02d} (UTC)')
else:
    print('failed!')

#
