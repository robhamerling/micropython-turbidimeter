##################################################################################
# coreInfo
# JarutEx (https://www.jarutex.com)
##################################################################################
import gc
import os
import sys
import time
import machine as mc

##################################################################################
# system setting
##################################################################################
gc.enable()
gc.collect()

##################################################################################
# show_hw_info()
##################################################################################
def show_hw_info():
    uname = os.uname()
    mem_alloc = gc.mem_alloc()
    mem_free = gc.mem_free()
    mem_total = mem_alloc + mem_free
    free_percent = "({:.1f}%)".format(mem_free/mem_total * 100.0)
    alloc_percent = "({:.1f}%)".format(gc.mem_alloc()/mem_total * 100.0)
    stat = os.statvfs('/flash')
    block_size = stat[0]
    total_blocks = stat[2]
    free_blocks  = stat[3]
    rom_total = total_blocks * block_size
    rom_free = free_blocks * block_size
    rom_usage = rom_total - rom_free
    rfree_percent = "({:.1f}%)".format(rom_free/rom_total * 100.0)
    rusage_percent = "({:.1f}%)".format(rom_usage/rom_total * 100.0)
    print("ID ............:", ":".join([f"{c:02x}" for c in mc.unique_id()]))
    print("Platform ......:", sys.platform)
    print("Version .......:", sys.version)
    print("Memory")
    print("   total ......: {:.1f} KB".format(mem_total / 1024))
    print("   usage ......: {:.1f} KB".format(mem_alloc / 1024), alloc_percent)
    print("   free .......: {:.1f} KB".format(mem_free / 1024), free_percent)
    print("ROM")
    print("   total ......: {:.1f} KB".format(rom_total / 1024))
    print("   usage ......: {:.1f} KB".format(rom_usage / 1024), rusage_percent)
    print("   free .......: {:.1f} KB".format(rom_free / 1024), rfree_percent)
    print("system name ...:", uname.sysname)
    print("node name .....:", uname.nodename)
    print("release .......:", uname.release)
    print("version .......:", uname.version)
    print("machine .......:", uname.machine)
    print("Frequency .....:", mc.freq())


##################################################################################
# is_prime()
##################################################################################
def is_prime(x):
    i = 2
    while (i < x):
        if x%i == 0:
            return False
        i = i+1
    if (i == x):
        return True
    return False

##################################################################################
# test_prime_number()
##################################################################################
def test_prime_number(maxN):
    counter = 0
    t0 = time.ticks_ms()
    for n in range(2, maxN):
        if is_prime(n):
            counter+=1
    t1 = time.ticks_ms()
    print("Found {} prime numbers in {} msecs.".format(counter,abs(t1-t0)))


##################################################################################
# main program
##################################################################################
try:
    show_hw_info()
    test_prime_number(2000)
except KeyboardInterrupt:
    pass
print("end of program")

#
