# ds3231.py driver for DS3231 module on ESP32 (e.g. for Turbidimeter)

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2024 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


Rob Hamerling, Version 4.0, January 2024

 Properties:
 2 major methods:
    - synchronize RTC with date/time from DS3231 (e.g. after a power-on reset)
    - synchronize DS3231 with current RTC value (e.g. to initialize the DS3231)
 Supports:
 - period 2000-2099 (century-bit in DS3231 fixed)
 - 24-hour mode
 - day-of-week, day-of-year ignored
 - methods return tuple: (year,month,day-of-month,hour,minute,second)
 - instantiate of DS3231 requires active I2C interface
"""

from machine import RTC, I2C
from micropython import const

DS3231_I2C_ADDR = const(0x68)               # default I2C address of DS3231

rtc = RTC()                                 # instance of system RealTime Clock

def bcd2dec(bcd):
    return (((bcd & 0xF0) >> 4) * 10 + (bcd & 0x0F))

def dec2bcd(dec):
    tens, units = divmod(dec, 10)
    return (tens << 4) + units

class DS3231:
    def __init__(self, i2c, addr=DS3231_I2C_ADDR):
        self._connected = False                         # initially not connected
        if not isinstance(i2c, I2C):                    # check for I2C instance
            print(f"{i2c} must be an I2C instance")
            return
        self._bus = i2c
        self._addr = addr
        self._buffer = bytearray(7)
        try:
            if self._addr in self._bus.scan():
                self._connected = True
        except:
            print("No DS3231 detected")

    def ds3231_to_rtc(self):
        # set system RTC from DS3231
        try:
            self._bus.readfrom_mem_into(self._addr, 0, self._buffer)   # date/time from DS3231
            self._connected = True
        except:
            print(f"Failed to read DS3231!")
            self._connected = False
        ss = bcd2dec(self._buffer[0])
        mm = bcd2dec(self._buffer[1])
        hh = bcd2dec(self._buffer[2] & 0x3f)            # assume 24-hour mode
        DD = bcd2dec(self._buffer[4])
        MM = bcd2dec(self._buffer[5] & 0x1f)            # assume 21st century
        YY = 2000 + bcd2dec(self._buffer[6])            # epoch start Micropython!
        rtc.datetime((YY, MM, DD, 0, hh, mm, ss, 0))    # synchronize RTC with DS3231
        return (YY, MM, DD, hh, mm, ss)

    def rtc_to_ds3231(self):
        # set DS3231 from RTC
        YY, MM, DD, _, hh, mm, ss, _ = rtc.datetime()   # actual system date/time
        self._buffer[0] = dec2bcd(ss)
        self._buffer[1] = dec2bcd(mm)
        self._buffer[2] = dec2bcd(hh)                  # 24hr mode
        self._buffer[3] = 0x00
        self._buffer[4] = dec2bcd(DD)                  # day of month
        self._buffer[5] = dec2bcd(MM) | 0x80           # century bit
        self._buffer[6] = dec2bcd(YY - 2000)
        try:
            self._bus.writeto_mem(self._addr, 0, self._buffer) # synchronize DS3231 with rtc
            self._connected = True
        except:
            print(f"Failed to write DS3231!")
            self._connected = False
        return (YY, MM, DD, hh, mm, ss)

    @property
    def isconnected(self):
        return self._connected

#
