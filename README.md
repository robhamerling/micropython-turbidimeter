# Turbidimeter - Version 4.0, Februari 2024

This project using Micropython programming language and a microcontroller
supporting **Micropython** (like an **ESP32**) is a follow-up of a similar
Arduino project using C/C++ as programming language and an Atmel Mega2560
microcontroller.

***This repository is under construction!***

## Hardware

The Turbidimeter is built on a 90x150 cm prototyping board (Vero stripboard).
The layout can be found in the **doc** directory

List of components:
-    1	Seeed Studio XIAO ESP32-C3 (with 2 7-pins header male)<br>
	    alternatively: XIAO RP2040 or XIAO ESP32-S3
-    1 	PCA9548A I2C bus expander (on PCB with 2 12-pins header male)
-    1 	OLED module (64x128 pixels) with I2C interface (with 4-pins 90º header male)
-    1  	DS3231 RTC module with battery backup (with header pins)
-    2	MCP23008 port expander + 18-pins PDIP socket
-    1	USB cable with USB-C connector 
-    1	PCB: stripboard 90x150mm (32 strips with 50 contacts, 2.54mm grid) 
-    2 	7-pins header female for ESP32
-    2 	12-pins header female for PCA9548 
-    1	5-pin header male for (small) DS3231 module
-    1	4 pins 90º header female for OLED module
-    8 	4-pins 90º header female for sensors and LEDs
-    16	series resistor for LEDs (150/180/220 Ohm)
-    1 	push button(‘start’) 
-	    silver wire

The ESP32 board is supposed to be powered from a USB port of a PC.
All other devices are powered from the ESP32 board.

Four sensors are supported, each consisting of:
-    1	TSL2591 sensor
-    1	IR LED
-    1  RGB LED 
-    2	4-wire cable for TSL2591 and LEDs
LEDs share the Ground wire of the Sensor.


## Turbidimeter software

- turbidi.py        - primary turbidimeter script
- turbidi.prm       - measurement parameters
- ds3231.py         - class for DS3231 module
- ds3231get.py      - synchronize RTC or ESP32 with DS3231 module
- ds3231set.py      - script to set DS3231 to UTC
- hardware_check.py - elementary test of turbidimeter hardware
- main.py           - (example) synchronize RTC with DS3231 at power-on/reset
- mcp23008.py       - class for MCP23008 port multiplexor
- oled.py           - class for text-only OLED display
- oled_font.py      - font for OLED display
- sys_info.py       - information about ESP32 hardware
- tca9548.py        - class for TCA9548 I2C bus multiplexor
- time.py           - extension of standard time module for local time
- tsl2591.py        - class for TSL2591 sensor

## Documentation

Subdirectory **doc** contains schematics, pictures sample log

- turbidimeter-stripboard.jpg - picture of assembled stripboard<br>
                       (serial resistors for IR LEDs not yet mounted!)
- turbidi.vrt       - VeroRoute source file for (stripboard) PCB
- turbidi4_a.png    - PCB layout
- turbidi4_b.png    - PCB layout
- turbidi4_c.png    - PCB layout


## Additional software

Subdirectory **util** contains some tools for testing components.
Executed on PC/Laptop:
- move-logs.py        - move log files from Turbidimeter to PC/Laptop
- syncrtc.py          - Set clocks of ESP32 (RTC and DS3231) to UTC 
- copy-all.py         - copy all program files to the ESP32.
Executed on ESP32:
- hardware_check.py   - functional tests of all turbidimeter components.


## Program Development Software

- With Windows installation of Python may be required (select 3.11 or newer).

- With Windows additional installation of Python may be required (select 3.11 or newer).

- With Windows: the USB to UART bridge controller on the ESP32 board
  is of type **CP2104**, which may require installation of an appropriate driver, visit
  - https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers
  and select the appropriate file in the Downloads section.
  After install determine which (virtual) serial port is created, e.g. COM3:

- Tool to transfer Micropython firmware to the ESP32 **esptool.py**
  - https://github.com/espressif/esptool
  - https://docs.espressif.com/projects/esptool/en/latest/esp32/installation.html

- Preferred program development: **Thonny** IDE
  - See https://thonny.org/ for installation and use, and
    https://randomnerdtutorials.com/getting-started-thonny-micropython-python-ide-esp32-esp8266/
    for a beginners introduction.

- Alternative program development tools:
    - Source code editor (preferrably with Python highlighting)
    - **uPyLoader** to load Python scripts into ESP32<br>
      (incl terminal interface)<br>
      Visit https://github.com/BetaRavener/uPyLoader
    - **mpremote**: a commandline interface to Micropython devices<br>
      install mpremote with: pip install mpremote

## User's Guide

- Connect the ESP32 to the PC with a USB data cable

- Install of the Micropython firmware:
  - Visit https://www.micropython.org/download/esp32c3/
  - Select the latest 'preview' ".bin" file for the ESP32-C3 of Espressif.<br> 
    The name of the firmware file starts with ESP32\_GENERIC\_C3
  - Follow the instructions on the top of that page on how to 
    transfer the firmware to the ESP32C3 with esptool.py

- Copy all .py files for the Turbidimeter to the ESP32:
  - Download all Turbidimeter files to a local directory or<br>
    git clone https://github.com/robhamerling/micropython-turbidimeter
  - Start Thonny
    - Open Tools -> Options -> Interpreter<br>
      Select interpretor: 'MicroPython (generic)'<br>
      Select Port (maybe automatically selected)
    - Open File and specify directory with Turbidimeter files
  - Open one-by-one all Turbidimeter .py files
    - Use from toolbar: File -> Save Copy -> MicroPython Device
    - Select or specify a filename
  - Alternatively use the script 'copy-all.py' in './util'
    (which may some adaptation for your particular system setup)
  - Set the DS3231 to **UTC** date/time<br>
    (follow the instructions in the script ds3231set.py)
  -Alternatively use the script 'syncrtc.py in './util'
  - **Note:** Thonny resets by default the RTC to local PC time.<br>
    To avoid this add the following line to the Thonny file
    'configuration.ini' under the section [ESP32]:<br>
        **sync_time = False**
    Under Linux this file can be found in '~/.config/Thonny/'


- Working with the Turbidimeter (online)

  - The Turbidi program can be used **online** (connected to a PC or
    laptop via a USB cable and a virtual serial port) or **offline** (stand-alone).
  - It is recommended to start in online mode (default installation)
    to become familiar with board and software.<br>
    *For offline use see the separate section below.*
  - Connect the turbidimeter via a USB cable to a PC or laptop.
  - Prepare the file turbidi.prm and transfer it to the ESP32.<br>
    The settings in the turbidi.prm file provided with this package are
    'dummies' which have to be adapted to the actual environment:
    a calibration fase before actual measurement.
  - Open a terminal/commandline session on the PC<br>
    or start a terminal emulation program.
  - Select the proper serial port.
  - Enter:<br>
    **   import turbidi**<br>
    to start the measurements.
  - Watch the output in the serial terminal window<br>
    and on the OLED screen.
  - When asked for enter a comment to identify this specific
    measurement. This comment will be added to the log file.
  - Push the start button to start the measurement.
  - Wait until the measurement completed<br>
    or interrupt the measurement (Ctrl-C).

  Notes:
    - Support for local time is provided by an extension of the
      standard micropython time module, but dedicated to
      Central Europe and its Daylight Savings Time standard.
    - The source files contain additional comments and
      more details!


- Copy Measurement results to PC:

  - Start Thonny
  - Use from toolbar: File -> Open -> MicroPython Device
  - Measurement results are stored in a .csv file<br>
    select a file ('*.csv')<br>
    (there may be several .csv files from previous runs!)
  - Use from toolbar: File -> Save Copy -> This computer
  - Select the desired destination location (directory)
  - Altenatively use the 'move-logs.py' script to move all
    log files (and delete these from the ESP32 memory).


- Interpretation of measurement results

  - The measurement results are stored in a CSV file.
  - The columns 'period', 'gain' and 'integration' show
    the parameters as obtained from the parameter file.
  - The column 'time' is the actual (local) time of the
    measurement, the column 'secs' is the relative time
    in seconds since the start of the measurements.
  - The columns 'cuvet-#' show the counts of the IR
    channel of the TSL2591s, whereby a value of '0'
    means that no TSL2591 was present and a value
    '-1' means overflow of the count of the IR channel.


- Working with the Turbidimeter offline (stand alone)

  - For offline use the file 'main.py' should been extended with
    a statement:<br>
        **import turbidi**<br>
    This statement is already present in main.py, but commented out!
  - With this statement active the turbidi program is always started
    automatically after a power-on or reset.
    It uses the actual .prm file currently in the ESP32
    (unless you changed it in an online session).
  - When measurements completed normally the turbidi program
    issues a reset, resulting in an automatic restart of
    the turbidi program, which will wait for the start-button for
    a new measurement cycle (presumably after exchanging cuvets!).
  - When the program is interrupted by a keyboard-interrupt
    (e.g. Ctrl-C) or after a non recoverable error condition
    it does not issue the reset and the ESP32 becomes reachable
    online for update of the .prm file, log transfer, etc.
  - When offline use is not desired anymore remove or comment-out
    the line with 'import turbidi' from main.py.


## Appendix

Turbidimeter Application Internals

Datacollection of the Turbidimeter program goes as follows:

1. The complete measurement is divided in a number of periods.
   Each period consists a number of intervals. 
   For every period the parameter file contains the duration of the period,
   the length of interval between consecutive measurements and the sensitivity
   settings of the TSL2591 light sensor: gain and integration time for each
   of the used colors (Infrared, Red, Green, Blue). 
   The parameters apply to every cuvet.
2. At the beginning of every interval a measurement as described in item 3
   is performed for each of the 4 cuvets immediately after each other. 
   For every cuvet a separate file is created for storing the measurement results.
3. For every cuvet the light of InfraRed, Red, Green and Blue LED is measured. 
   Both the Visual and the InfraRed counts of the TSL2591 are collected.
   For each of the 4 colors:
   - switch-on the appropriate LED
   - set gain and integration of TSL2591 for the specific LED color
   - wait for completion of the (first) integration (ADC)
   - read both counts of the TSL2591
   - switch-off the LED
   - store the counts in the log file.


#
