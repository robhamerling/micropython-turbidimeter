#!/usr/bin/env python3

# move all turbidi log (*.csv) files to local directory on PC/laptop.
# This script should be executed on the PC, with installed 'mpremote'.

import sys
import subprocess

dst = "../"         # destination of moved logs

try:
    log = subprocess.run(["mpremote", "fs", "ls"],
            check=True, capture_output=True).stdout
except subprocess.CalledProcessError as e:
    print(e.output)
    sys.exit(1)

for ln in log.decode().splitlines():    # lines in list
    ln = ln.strip()
    if ln.endswith(".csv"):             # only filetype to handle
        fn = ln.split(" ")[-1]          # filespec
        try:
            # copy remote to destination
            log = subprocess.run(["mpremote", "fs", "cp", ":"+fn, dst],
                    check=True, capture_output=True).stdout
            # remove remote (if copy finished successfully)
            log = subprocess.run(["mpremote", "fs", "rm", ":"+fn],
                    check=True, capture_output=True).stdout
        except subprocess.CalledProcessError as e:
            print(e.output)
        print(fn)

#
