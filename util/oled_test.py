# test I2C oled display (sh1106 or ssd1306)

from machine import I2C, SoftI2C, Pin
import sys

import oled                             # driver
import oled_cycle                       # tests

# Select I2C bus hardware and wiring dependent!
if sys.platform == "mimxrt":            # TEENSY40
    bus = I2C(0)                        # hardware I2C
    # bus = SoftI2C(sda=Pin(18), scl=Pin(19))     # software I2C
elif sys.platform == "esp32":
    bus = I2C(0)
    # bus = SoftI2C(sda=Pin(19), scl=Pin(18))     # software I2C
elif sys.platform == "esp8266":
    bus = SoftI2C(sda=Pin(12), scl=Pin(14))
    # bus = SoftI2C(sda=Pin(12), scl=Pin(14))     # software I2C


print("Detected I2C devices at:", bus.scan())
dsp = oled.OLED(bus)                   # instance with default settings
print("Connection to OLED at I2C address 0x{:02X} ".format(dsp.address), end="")
if dsp.connected:                      # OLED is apparently present
    print("succeeded!\nHit Ctrl-C to terminate.")
    oled_cycle.cycle(dsp)              # the actual display test
else:
    print("failed!")
print("End of OLED tests")
