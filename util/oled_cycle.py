
# generic test procedure of oled screens
# can be used for SH1106 and SSD1306 OLED displays.

import time

def cycle(oled):

    def hello(l):               # number of lines
        # RTC assumed to present UTC, CET is 3600 seconds later (in winter)
        _, _, _, hh, mm, ss, _, _ = time.localtime(time.time() + 3600)
        for i in range(l):
            oled.string("{:d} now {:2d}:{:02d}:{:02d}".format(i+1, hh, mm, ss), i, i // 4)
            time.sleep(.2)

    try:
        while True:
            oled.clear()
            oled.reverse_video(False)
            oled.flip(False)

            hello(8)            # lines with text
            time.sleep(2)

            oled.clear()
            oled.flip()
            hello(8)            # lines with text
            time.sleep(2)

            oled.clear(6)       # line 6 to last line
            time.sleep(2)
            oled.clear(1,2)     # lines 1 and 2
            time.sleep(2)

            oled.reverse_video(True)
            time.sleep(2)

    except KeyboardInterrupt:
        print("Interrupted from keyboard")

    finally:
        # back to default mode
        oled.flip(False)
        oled.reverse_video(False)
        oled.clear()

#
