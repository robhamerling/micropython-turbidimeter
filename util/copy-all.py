#!/usr/bin/env python3

# copy/replace all required turbidimeter files to ESP32
# This script should be executed on the PC, with installed 'mpremote'.

import os
import sys
import subprocess

# location of turbidimeter files
src = os.path.join("/", "media", "nas", "micropython-turbidimeter")

# to be copied to target:
filelist = ("ds3231.py",
            "ds3231set.py",
            "ds3231get.py",
            "hardware_check.py,
            "main.py",
            "oled.py",
            "oled_font.py",
            "mcp23008.py",
            "sys_info.py",
            "time.py",
            "tca9548.py",
            "tsl2591.py",
            "turbidi.prm",
            "turbidi.py",
            )

for f in filelist:
    try:
        filespec = os.path.join(src, f)
        log = subprocess.run(["mpremote", "fs", "cp", filespec, ":"],
                check=True, capture_output=True).stdout
        print(log.decode(), end="")
    except subprocess.CalledProcessError as e:
        print(e.output)


#
